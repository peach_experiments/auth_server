FROM python:3.10

# Create working directory
WORKDIR /usr/src/app

# Copy dependency file
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy dotenv
COPY .env .

# Copy the python app
COPY ./src .

# Run app
CMD [ "python3", "main.py" ]
