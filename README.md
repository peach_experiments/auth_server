# Auth_Server

## Description
Authentication / Authorization server used to login to my personal website  
projects.

## Installation

### Docker
Set environment variables in a `.env` file and launch the container
```
  docker compose up -d
```

## Environlment Variables
### APP_HOST
IP Address of the host

Default: 0.0.0.0

### APP_PORT
Port number of the host

Default: 8080

### APP_WORKERS
Number of Uvicorn Workers

Default: 2

## Support
You're on your own, this software is supplied as is.  Either you get it or you  
don't.

If you do get it, please let me know, I have some questions.

## Roadmap
Step one: 
 - issue authenticated JWT

Step two: 
 - integrate with LDAP server

Step three: 
 - profit? 
 - ... 
 - ?

## Contributing
Not welcoming contributions at this time. Thanks for asking though.

## Authors and acknowledgment
Me! For not knowing what I'm doing until after I'm done.

## License
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
