#! ./venv/bin/python3

import uvicorn
from fastapi import FastAPI
from config import Config
from routers import auth

app = FastAPI()

app.include_router(auth.router)


@app.get("/", status_code=200)
def root():
    print("Hello, FastAPI")


def main():
    conf = Config()
    print("Hello, World")
    start_api()


def start_api():
    print("Host", Config().APP_HOST)
    print("Port", Config().APP_PORT)
    print("Workers", Config().APP_WORKERS)
    uvicorn.run('main:app',
                host=Config().APP_HOST,
                port=int(Config().APP_PORT),
                workers=int(Config().APP_WORKERS))


if __name__ == '__main__':
    main()
