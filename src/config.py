import os
import dotenv

class Config(object):

    def __new__(cls):
        # Check if instance exists
        if not hasattr(cls, '_instance'):
            # Save singleton so init runs once
            cls._instance = super(Config, cls).__new__(cls)
        # Retrurn single instance
        return cls._instance

    def __init__(self):
        # Get all environment variables
        for key, value in os.environ.items():
            setattr(self, key, value)

        # Get all dotenv values
        dotenv.load_dotenv()

        # TODO: Get all config.ini values
