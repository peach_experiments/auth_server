import jwt
import time
from fastapi import APIRouter
from pydantic import BaseModel
from config import Config

router = APIRouter(prefix="/auth")


LIFE_SPAN = 1800
ISSUER = "PEACH"


class AuthReq(BaseModel):
    username: str
    password: str
    client_id: str
    client_secret: str


@router.post("/", status_code=200)
def auth(auth_req: AuthReq):
    username = auth_req.username
    password = auth_req.password
    client_id = auth_req.client_id
    client_secret = auth_req.client_secret

    print (username, password, client_id, client_secret)

#    if not authenticate_user_credentials(username, password):
#        # TODO: 401 error
#        return {"error": "access_denied"}
#
#    if not authenticate_client(client_id, client_secret):
#        # TODO: 400 error
#        return {"error": "invalid_client"}
#
#    access_token = generate_access_token()
#
#    return {"access_token": access_token,
#            "token_type": "JWT",
#            "expires_in": LIFE_SPAN}


def authenticate_user_credentials(username, password):
    return True


def authenticate_client(client_id, client_secret):
    return True


def generate_access_token():
    return True
#    payload = {
#        "iss": ISSUER,
#        "exp": time.time() + LIFE_SPAN
#    }
#
#    access_token = jwt.encode(payload,
#                              private_key,
#                              algorithm="RS254")
#
#    return access_token.decode()

# TODO: Make async
